from aqms_api import Configuration as CC
from aqms_api import Input_manager as IM
import pytz
import datetime as dtime
import logging

###########################################################################################
def Init_logger():
    loggername='input_manager'
    logger = logging.getLogger(loggername)
    logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
    fileh = logging.FileHandler(loggername+'.log')
    fileh.setLevel(logging.DEBUG)
# create console handler with a higher log level
    consoleh = logging.StreamHandler()
    consoleh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
    formatterfile    = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatterconsole = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    fileh.setFormatter(formatterfile)
    consoleh.setFormatter(formatterconsole)
# add the handlers to the logger
    logger.addHandler(fileh)
    logger.addHandler(consoleh)
    return logger  

###########################################################################################
###########################################################################################
if __name__ == '__main__':
    """
    Interface to get the OBS data from DPE Air Quality stations
    
    first you have to load the environment
    source Scripts/activate_sdc.sh
    
    It can return the list of stations with the export_stations() method
    It can return the whole list of OBS, in a dictionnary format

    the dictionary contains the following keys:
        "obs_data" : OBS data in a pandas table format,
        "obs_column_names" name of the data columns of the pandas dataframe,
        "species_properties_dict": dictionnary that contains the chemical species properties from the stations.
    
    Obs_save_dir
        directory where to ssave the raw and processed OBS data
        
    use_file_from_previous_request 
        is a True/False switch, can be used if a identcal previous request has been already done, 
        and loads the raw data file instead of submitting a request to the api
        note: the raw data file is always saved in the Obs_save_dir
        
    save_obs_processed_data
        is a True/False switch, to trigger the save in a csv file of the processed OBS data in Obs_save_dir
    
    var_to_export
        list of variable to get from the api
        so far the possible list is :
        ["Wind", "CO", "NO2", "PM10", "NEPH", "CTM", "T", "RH", 'HUMID', 'NH3', 'NO', "O3", 
           'PM10d', 'PM2.5', 'PM2.5d', 'RAIN', 'SD1', 'SO2', 'SOLAR', 'TSPd']

    **********
    IMPORTANT
    The values are shifted to the beginning of the hour, not at the end like native DPE station data
    ********** 
    """
    justif = 102
    logger = Init_logger()
################  
   
    start_date_aest = dtime.datetime(2023,1,1,0)
    # end_date_utc = start_date_utc + dtime.timedelta(days=2)
    end_date_aest = dtime.datetime(2023,1,3,0)
    # timestamp_aedt = dtime.datetime.now()
    timestamp_aedt = dtime.datetime.strptime('2023012014', '%Y%m%d%H')

    # where to save the file
    Obs_save_dir = '.'

###########################################################
    Configuration = CC.Configuration_Class(logger, justif,
            start_date_aest, end_date_aest, timestamp_aedt,
            Obs_save_dir,
            )

    ##### input configuration
    use_file_from_previous_request = True
    save_obs_processed_data = True

    var_to_export = ["O3", "Wind", "NO2"]

    dpie_input_station_list = ["BRINGELLY", "CAMPBELLTOWN_WEST", "CAMDEN", "LIVERPOOL"]

    Configuration.configure_request(
                            var_to_export,  
                            dpie_input_station_list, 
                            use_file_from_previous_request,
                            save_obs_processed_data,
                            )

    IMC = IM.Input_manager_Class(Configuration)
    
    #get the stations
    stations = IMC.export_stations()
    print(stations)
    
    # get the OBS data
    input_data_dict = IMC.import_obs()
    
    print(input_data_dict)




 



