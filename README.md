# AQMS API #
===================================


### Interface to get the OBS data from DPE Air Quality stations ###
---------------------------------------------------------------------
This is a small python module to import as pandas table the OBS from the NSW DPE Air Quality stations.


### How do I get set up? ###
------------

Interface to get the OBS data from DPE Air Quality stations
    
+ **First** you have to **load the environment**
    source Scripts/activate_sdc.sh
    
+ **Outputs**    
    * It can return the **list of stations** with the **export_stations()** method
    * It can return the whole list of OBS, in a dictionary format

    * **the dictionary** contains the following keys:
        - *"obs_data"* : OBS data in a pandas table format,
        - *"obs_column_names"* name of the data columns of the pandas dataframe,
        - *"species_properties_dict"*: dictionnary that contains the chemical species properties from the stations.

+ **Input parameters**
    * Obs_save_dir
        * Directory where to save the raw and processed OBS data
        
    * use_file_from_previous_request 
        * is a True/False switch, can be used if a identcal previous request has been already done, 
        and loads the raw data file instead of submitting a request to the api
        * note: the **raw data** file is **always saved** in the Obs_save_dir
        
    * save_obs_processed_data
        * is a True/False switch, to trigger the save in a csv file of the processed OBS data in Obs_save_dir
    
    * var_to_export
        * list of variable to get from the api
            so far the possible list is :
            
            ["Wind", "CO", "NO2", "PM10", "NEPH", "CTM", "T", "RH", 'HUMID', 'NH3', 'NO', "O3", 
            'PM10d', 'PM2.5', 'PM2.5d', 'RAIN', 'SD1', 'SO2', 'SOLAR', 'TSPd']

**********
**IMPORTANT**
+ **The values are shifted to the beginning of the hour**, not at the end like native DPE station data
********** 

### Contribution guidelines ###

### Who do I talk to? ###

* xavier.barthelemy@environment.nsw.gov.au
* Air Quality Research Team of DPE