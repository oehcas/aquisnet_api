"""
.. module:: Configuration for aquisnet_api
   :platform: Unix
   :synopsis: contains the routine to configure the request for aquisnet_api.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import os
import sys
from . import DateMagics as DM

###########################################################################################
class Configuration_Class(object):
    ''' This class configure the whole Forecast.
    '''
    def __init__(self, logger, justif,
             start_date_aest, end_date_aest, timestamp_aedt,
             Main_output_dir,
             **kwargs 
             ):

        self.logger = logger
        #justification of the messages
        self.justif = justif


       
        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.info('Configuring aquisnet api requests'.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))
        
        
        ######
        # Date magics!
        self.start_date_utc, self.start_date_aest, self.start_date_aedt = DM.DateMagics(start_date_aest, 'AEST')
        self.end_date_utc, self.end_date_aest, self.end_date_aest = DM.DateMagics(end_date_aest, 'AEST')
        self.timestamp_utc, self.timestamp_aest, self.timestamp_aedt = DM.DateMagics(timestamp_aedt, 'AEDT')

        ######
        #  DL model parameters
        self.var_to_export = None

        self.dpie_input_station_list = None

        self.var_data_list_from_input_pd = None

        ######
        #  input parameters
        self.specie_properties_dict = None

        self.use_file_from_previous_request = None
        self.save_obs_processed_data = None
        ######
        # Output parameters
        self.Main_output_dir = Main_output_dir

        return
############################################################################################################################
    def configure_request(self, 
                            var_to_export,  
                            dpie_input_station_list,
                            use_file_from_previous_request,
                            save_obs_processed_data,
                            
                            ):
        self.var_to_export = var_to_export

        self.dpie_input_station_list = dpie_input_station_list
        self.save_obs_processed_data = save_obs_processed_data
        self.use_file_from_previous_request = use_file_from_previous_request

        ##########
        # var format for file
        if (len(self.var_to_export) > 1):
                self.var_to_export_dir = '_'.join(self.var_to_export)
        else: 
                self.var_to_export_dir = self.var_to_export[0]
        return

###########################################################################################
    def configure_input(self, Data_input_flow, use_file,
                        save_obs_processed_data):
        """
        configure input parameter
        """
        self.Data_input_flow = Data_input_flow
        self.use_file = use_file

        return
###########################################################################################

