"""
..  module:: Input_manager
    :platform: Unix
    :synopsis: Definition of the basic object class to output .

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
.. moduleauthor:: Hubert Nguyen <hubert.nguyen@environment.nsw.gov.au>
   
"""

import sys
import os
import stat
import numpy as np
import pandas as pd
import pytz
import datetime as dtime
import itertools

from . import aqms_api 

###########################################################################################
class Input_manager_Class(object):
    """ 
    This class defines a Input_Class, that contains the capacity to manage the inputs.

    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output.   

       
    """
    def __init__(self, Configuration,
                ):

        self.Configuration = Configuration
        self.logger = self.Configuration.logger
        self.justif = self.Configuration.justif

        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.info('Configuring the Input manager'.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))


        self.var_to_export =  self.Configuration.var_to_export


        self.use_file_from_previous_request = self.Configuration.use_file_from_previous_request
        self.OBS_save_file_template = 'Allobs_{raw_processed}_{var}_date_{date_start}_{date_end}.csv'

        self.aedt = pytz.timezone('Australia/Sydney')
        self.aest = pytz.timezone('Australia/Brisbane')

        return

###########################################################################################
    def MakeDir(self, ddir):
        ''' This function makes the different working directories
        '''
        if not os.path.exists(ddir):
            os.makedirs(ddir)
            mod775 = stat.S_IRUSR |stat.S_IWUSR |stat.S_IXUSR |stat.S_IRGRP |stat.S_IWGRP  |stat.S_IXGRP |stat.S_IROTH |stat.S_IXOTH
            os.chmod(ddir,mod775)
            #os.chmod(self.RunDir,0775)
            self.logger.info('Directory = {msg}'.format(msg=ddir).ljust(self.justif-7,'.') + 'CREATED')
        return    


# ###########################################################################################
    def clean_folder(self, folder_path):
        if os.path.exists(folder_path):
            print(f"The folder '{folder_path}' exists.")
        
            for filename in os.listdir(folder_path):
                file_path = os.path.join(folder_path, filename)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                        print(f"Deleted file: {file_path}")
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                        print(f"Deleted directory: {file_path}")
                except Exception as e:
                    print(f"Error deleting {file_path}: {str(e)}")
        else:
            return

###########################################################################################
    def Change_permissions(self, path):
        ''' This function makes the different working directories
        '''
        import stat
        import os
        os.umask(0)
        mod664 = stat.S_IRUSR |stat.S_IWUSR |stat.S_IRGRP |stat.S_IWGRP  |stat.S_IROTH  
        mod666 = stat.S_IRUSR |stat.S_IWUSR |stat.S_IRGRP |stat.S_IWGRP  |stat.S_IROTH |stat.S_IWOTH
        # os.chmod(path, mod664)
        os.chmod(path, mod666)
        return    

###########################################################################################
    def export_stations(self, ):
        self.AQMS =  aqms_api.aqms_api_class()

        pd.set_option('display.max_rows', 500)
        ######################
        # get stations site details and make a dict out of it
        AllSites = self.AQMS.get_site_details()
        # print(AllSites)
        # asdsa
        dpe_station_site_detail_pd = pd.json_normalize(AllSites.json())
        dpe_station_site_detail_pd["SiteName"] = dpe_station_site_detail_pd.loc[:,"SiteName"].replace(" ", "_", regex=True)
        dpe_station_site_detail_pd.set_index("SiteName", drop=True, inplace=True)
        # drop the duplicated station names
        dpe_station_site_detail_pd = dpe_station_site_detail_pd[~dpe_station_site_detail_pd.index.duplicated(keep='first')]
        
        dpe_station_site_detail_pd.sort_index(inplace = True)
        
        return dpe_station_site_detail_pd
###########################################################################################
    def load_dpe_api_input(self, ):
        ''' 
        This function connects with the dpe api to get station data.
        
        '''
        self.AQMS =  aqms_api.aqms_api_class()

        pd.set_option('display.max_rows', 500)
        ######################
        # get stations site details and make a dict out of it
        AllSites = self.AQMS.get_site_details()
        # print(AllSites)
        # asdsa
        self.dpe_station_site_detail_pd = pd.json_normalize(AllSites.json())
        self.dpe_station_site_detail_pd["SiteName"] = self.dpe_station_site_detail_pd.loc[:,"SiteName"].replace(" ", "_", regex=True)
        self.dpe_station_site_detail_pd.set_index("SiteName", drop=True, inplace=True)
        # drop the duplicated station names
        self.dpe_station_site_detail_pd = self.dpe_station_site_detail_pd[~self.dpe_station_site_detail_pd.index.duplicated(keep='first')]

        # print(self.dpe_station_site_detail_pd.sort_index(axis="index"))
        self.dpe_station_site_detail_dict = self.dpe_station_site_detail_pd.to_dict(orient='index')
        
        self.dpe_reverse_station_site_detail_dict = {}
        for key,val in self.dpe_station_site_detail_dict.items():
            self.dpe_reverse_station_site_detail_dict[val["Site_Id"]] = key

        # print(self.dpe_station_site_detail_dict)
        # print(self.dpe_reverse_station_site_detail_dict)

        ######################
        # get stations parameters and make a dict out of it
        Allparameters = self.AQMS.get_parameters_details()
        self.dpe_station_parameters_pd = pd.json_normalize(Allparameters.json())
        # grab hourly values and hourly averages
        mask = (
            (self.dpe_station_parameters_pd["SubCategory"] == "Hourly") 
            & (self.dpe_station_parameters_pd["Frequency"] == "Hourly average")
            & (self.dpe_station_parameters_pd["Category"] == "Averages")
        )
        
        self.dpe_station_parameters_dict = self.dpe_station_parameters_pd[mask].set_index("ParameterCode", drop=True, inplace=False).to_dict(orient='index')
        # print(self.dpe_station_parameters_dict)

        dpe_species_properties_dict = {}
        for spec, val in self.dpe_station_parameters_dict.items():
            dpe_species_properties_dict[spec] = {
                'Units': val['Units'],
                'Species_name':val['ParameterDescription'],
                'Units_name':val['UnitsDescription'],
            }

        
        ######################
        # build variable list to get from api
        # correspondance dict between input and api var name necessary
        self.dpe_station_available_variable_list = self.dpe_station_parameters_dict.keys()
        # print(self.dpe_station_available_variable_list)
        
        # ['CO', 'HUMID', 'NEPH', 'NH3', 'NO', 'NO2', 'OZONE', 'PM10', 'PM10d', 'PM2.5', 'PM2.5d', 
        # 'RAIN', 'SD1', 'SO2', 'SOLAR', 'TEMP', 'TSPd', 'WDR', 'WSP']
        
        self.dpe_var_selection_dict = {
            "Wind" : ['WDR', 'WSP'],
            "CO" : ["CO"],
            "NO2" : ["NO2"],
            "PM10" : ["PM10"],
            "NEPH" : ["NEPH"],
            "CTM" : ["CTM"],
            "T" : ["TEMP"],
            "RH" : ["RH"],
            'HUMID' :['HUMID'],
            'NH3' : ['NH3'],
            'NO' : ['NO'],
            "O3" : ['OZONE'],
            'PM10d' : ['PM10d'],
            'PM2.5' : ['PM2.5'],
            'PM2.5d' : ['PM2.5d'],
            'RAIN' : ['RAIN'],
            'SD1' : ['SD1'],
            'SO2' : ['SO2'],
            'SOLAR' : ['SOLAR'],
            'TSPd' : ['TSPd'],
            }

        dpe_selected_var_list = sorted(list(
            itertools.chain.from_iterable(
                [self.dpe_var_selection_dict[ii] for ii in self.Configuration.var_to_export] )
                ))

        # print(dpe_selected_var_list)

        ######################
        # build station list to get from api

        dpe_selected_station_id = [
            self.dpe_station_site_detail_dict[ii]["Site_Id"] for ii in self.Configuration.dpie_input_station_list
        ]
        # make list unique
        dpe_selected_station_id = list(set(dpe_selected_station_id))

        # print(dpe_selected_station_id)

        ######################
        # build the api request
        ObsRequest = self.AQMS.ObsRequest_init()
        ObsRequest['Categories'] = ["Averages"]
        ObsRequest['SubCategories'] = ["Hourly"]
        ObsRequest['Frequency'] = ["Hourly average"]
        ObsRequest['Parameters'] = dpe_selected_var_list 
        ObsRequest['Sites'] = dpe_selected_station_id

        ObsRequest['StartDate'] = self.Configuration.start_date_aest.strftime('%Y-%m-%d')
        ObsRequest['EndDate'] = self.Configuration.end_date_aest.strftime('%Y-%m-%d')

        ########################
        # Computing raw file names for in/out

        OBS_raw_save_file = self.OBS_save_file_template.format(
            raw_processed = "raw",
            var = self.Configuration.var_to_export_dir,
            date_start = self.Configuration.start_date_aest.strftime("%Y%m%d"), 
            date_end = self.Configuration.end_date_aest.strftime("%Y%m%d"), 
        )
        dir = self.Configuration.Main_output_dir

        filename_full_path = os.path.join(dir, OBS_raw_save_file)
        ########################
        
        if self.use_file_from_previous_request:
            self.logger.info('Using OBS raw data file'.ljust(self.justif,'.'))
            # print("----------------------------------")
            # print(OBS_save_file)
            AllObs_pd = pd.read_csv(filename_full_path, index_col=0)
            # print(AllObs_pd.tail())
            self.logger.info ("Total row of API downloaded data = {num}".format(num=len(AllObs_pd)))
            # print("----------------------------------")
            self.logger.info('OBS raw data = {msg}'.format(
                msg=OBS_raw_save_file
                ).ljust(self.justif-6,'.') + 'LOADED')
            
            
        else:    
            self.logger.info('Downloading DPE OBS data from API'.ljust(self.justif,'.'))
            # Set a maximum number of retry attempts
            max_attempts = 5
            current_attempt = 1
            ### Download again data for max_attempts
            while current_attempt <= max_attempts:
                # print(ObsRequest)
                AllObs = self.AQMS.get_historical_obs(ObsRequest)
                # print("Status code = ", AllObs.status_code)
                
                
                if AllObs.status_code == 200:
                    AllObs_pd = pd.json_normalize(AllObs.json())
                    self.MakeDir(dir)
                    AllObs_pd.to_csv(filename_full_path)
                    self.Change_permissions(filename_full_path)
                    self.logger.info('OBS raw data = {msg}'.format(msg=OBS_raw_save_file).ljust(self.justif-7,'.') + 'WRITTEN')
                    # print(AllObs_pd.tail())
                    break
                    
            
                else:
                    self.logger.error("Attempt num={num} to download API data failed".format(num=current_attempt))
                    current_attempt += 1

                    
                    
            if current_attempt > max_attempts:
                raise ValueError("Failed to retrieve data from API, error=",AllObs.status_code)
        

        ### specifies the desired data types for four columns:  Hour, Value, Parameter.ParameterCode, and Site_Id
        dpe_Obs_dtype_dict = {
            "Hour"          : 'Int64',
            'Value'         : 'Float64',
            'Parameter.ParameterCode'  : 'string',
            'Site_Id'    : 'Int64',
        }
        AllObs_pd.astype(dpe_Obs_dtype_dict)
        # print("Columns of raw data:", AllObs_pd.columns)
      
        ######################
        # rearrange the pd to fit input format

        # print(AllObs_pd.loc[:,['DeterminingPollutant', 'Parameter.ParameterCode']])
        AllObs_pd["Date"] = pd.to_datetime(AllObs_pd['Date'], format='%Y-%m-%d')

        #remove 1 hour to set the values on the beginning of the hour
        AllObs_pd['Hour'] = pd.Series(
            [pd.Timedelta(hours=(ii-1)) for ii in AllObs_pd['Hour'].values]
            )
        # set index to dateime and localise it in AEST
        AllObs_pd.set_index(
            pd.DatetimeIndex((AllObs_pd["Date"] + AllObs_pd['Hour'])).tz_localize('Australia/Brisbane'), inplace=True
        )
        AllObs_pd.index.name = "datetime"
        
        columns_to_keep = ["Site_Id", "Parameter.ParameterCode", "Value"]
        Processed_obs_pd = AllObs_pd.loc[:,columns_to_keep]
        Processed_obs_pd["site_name"] = [self.dpe_reverse_station_site_detail_dict[ii] for ii in Processed_obs_pd["Site_Id"]]
        Processed_obs_pd["Var_name"] = Processed_obs_pd[["Parameter.ParameterCode","site_name"]].agg('_'.join, axis=1)
        columns_to_keep = ["Var_name", "Value"]
        Processed_obs_pd = Processed_obs_pd.loc[:,columns_to_keep]
        Processed_obs_pd = Processed_obs_pd.set_index(["Var_name",], drop=True, append=True).unstack(level="Var_name", fill_value=0)
        Processed_obs_pd.columns = [col[-1] for col in Processed_obs_pd.columns]
        Processed_obs_pd.sort_index(axis="columns", inplace=True)
        # print(Processed_obs_pd.describe())
        
        ######################
        # name input 
        # input 
        input_column_names = list(Processed_obs_pd.columns)
        # output
        self.dpe_selected_output_var_list = sorted(list(
            itertools.chain.from_iterable(
                [self.dpe_var_selection_dict[ii] for ii in self.Configuration.var_to_export] )
                ))
        
       
        print("-"*50)
        print ("input_column_names:")
        print (input_column_names)
        print("-"*50)
        
        return  Processed_obs_pd, input_column_names, dpe_species_properties_dict, dpe_selected_var_list

###########################################################################################
    def save_processed_data(self, input_data_pd,):
        """_summary_
        Save obs processed data 
        Returns:
            _type_: _description_
        """
        OBS_raw_save_file = self.OBS_save_file_template.format(
            raw_processed = "processed",
            var = self.Configuration.var_to_export_dir,
            date_start = self.Configuration.start_date_aest.strftime("%Y%m%d"), 
            date_end = self.Configuration.end_date_aest.strftime("%Y%m%d"), 
        )
        dir = self.Configuration.Main_output_dir

        filename_full_path = os.path.join(dir, OBS_raw_save_file)
        input_data_pd.to_csv(filename_full_path)

        
        return        

###########################################################################################
    def import_obs(self,):
        """
        main function to run to trigger the input data flow
        """
      
        input_data_pd, input_column_names, dpe_species_properties_dict, var_data_list_from_input_pd = self.load_dpe_api_input()

     
        input_data_dict = {
            "obs_data" : input_data_pd,
            "obs_column_names" : input_column_names,
            "species_properties_dict": dpe_species_properties_dict
        }
        ### update Configure
        self.Configuration.var_data_list_from_input_pd = var_data_list_from_input_pd

        ###############
        # saving data
        if self.Configuration.save_obs_processed_data:
            self.save_processed_data(input_data_pd,)

        self.logger.info("DPE station api data".ljust(self.justif - 2 ,'.') + 'OK')

        return input_data_dict
###########################################################################################

###########################################################################################
if __name__ == '__main__':
    import logging
    from . import Configuration as CC
    justif = 102
################ logger init
    loggername='input_manager'
    logger = logging.getLogger(loggername)
    logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
    fileh = logging.FileHandler(loggername+'.log')
    fileh.setLevel(logging.DEBUG)
# create console handler with a higher log level
    consoleh = logging.StreamHandler()
    consoleh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
    formatterfile    = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatterconsole = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    fileh.setFormatter(formatterfile)
    consoleh.setFormatter(formatterconsole)
# add the handlers to the logger
    logger.addHandler(fileh)
    logger.addHandler(consoleh)
################  
   
    start_date_aest = dtime.datetime(2023,1,1,0)
    # end_date_utc = start_date_utc + dtime.timedelta(days=2)
    end_date_aest = dtime.datetime(2023,1,3,0)
    # timestamp_aedt = dtime.datetime.now()
    timestamp_aedt = dtime.datetime.strptime('2023012014', '%Y%m%d%H')

    Obs_save_dir = '.'

###########################################################
    Configuration = CC.Configuration_Class(logger, justif,
            start_date_aest, end_date_aest, timestamp_aedt,
            Obs_save_dir,
            )

    ##### input configuration
    Data_input_flow = ["DPE_station_api"]
    # self.Data_input_flow = ["Artificial_forecast",]

    use_file_from_previous_request = True
    save_obs_processed_data = True

    var_to_export = ["O3", "Wind", "NO2"]

    dpie_input_station_list = ["BRINGELLY", "CAMPBELLTOWN_WEST", "CAMDEN", "LIVERPOOL"]

    Configuration.configure_request(
                            var_to_export,  
                            dpie_input_station_list, 
                            use_file_from_previous_request,
                            save_obs_processed_data,
                            )

    IMC = Input_manager_Class(Configuration)
    stations = IMC.export_stations()
    print(stations)
    input_data_dict = IMC.import_obs()




 



